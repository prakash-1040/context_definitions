#!/bin/bash

set -x

CURRENT_WS=`pwd`
WORKSPACE=$CURRENT_WS
MOD=org.openbravo.core2
CONTEXT=$(cat config/Openbravo.properties | grep -i context.name= |cut -d "=" -f2)

cd $WORKSPACE

npm install --legacy-peer-deps

# cypress report
if [ ! -d modules/$MOD ] ; then
   echo "Error Missing module org.openbravo.core2"
   exit 1
else
   git -C modules/$MOD checkout -f
   cd $WORKSPACE/modules/$MOD
   npm install --legacy-peer-deps
   TEST_WS="$WORKSPACE/modules/$MOD/web-jspack/org.openbravo.core2/src-test"
   cd $TEST_WS
   cp cypress.env.json.template cypress.env.json
   # execute test
   cd ../
   npm run instrument-code

   tomcat_context=$(cat  $WORKSPACE/config/Openbravo.properties|grep context.name| cut -d "=" -f2)
   echo -e "{\nREACT_APP_SERVER_URL=http://localhost:8080/$tomcat_context\n}" > $WORKSPACE/modules/org.openbravo.core2/web-jspack/org.openbravo.core2/.env
   npm_status="not running"
   node_pid="$(ps -e |grep node | cut -d '?' -f1)"
   [ "$node_pid" != "" ] && kill -8 $node_pid
   nohup npm start > $WORKSPACE/SANDBOX/npm_start.log &
   # sleep is required to make the dev environment start, which takes 2 to 3 minutes.
   echo sleep 3 minutes
   sleep 200
   #until [ "$npm_status" == "Compiled successfully" ]
   #do
   #	   sleep 5
   #	   npm_status=$(cat npm_start.log | grep "Compiled successfully"| uniq)
   #done
   cd $TEST_WS
   export DISPLAY=:3
   #time npx cypress run --browser chrome --env coverage=true
   SPEC_PATH="$WORKSPACE/modules/org.openbravo.pos2.restaurant/**/runFeaturesSpecsRestaurant.spec.js,$WORKSPACE/modules/org.openbravo.pos2/**/runFeaturesSpecsPOS2.spec.js,$WORKSPACE/modules/org.openbravo.pos2/**/runIntegrationSpecsPOS2.spec.js"
   $WORKSPACE/node_modules/cypress/bin/cypress run \
           --spec $SPEC_PATH \
           --browser chrome \
           --env coverage=true
fi

echo "Merge codecoverage reports"
cd $WORKSPACE/modules/org.openbravo.core2/web-jspack/org.openbravo.core2
npm run merge-reports

node_pid="$(ps -e |grep node | cut -d '?' -f1)"
[ "$node_pid" != "" ] && kill -9 $node_pid
exit 0
